---
title: TableForm | admin-design-vue
description: AdTableForm 组件的文档
---

# AdTableForm组件

通过配置JSON快速构建`表单列表`，支持动态添加删除，动态验证

## 按钮权限控制

[如何设置权限数据](https://fe-hl.github.io/document/pages/permissionDirective.html)


## 标准的列表的数据结构

```js
const res = {
  data: {
    records: [],
  },
};
```

## 表单列表配置

:::demo
tableForm/index
:::


## 自定义只读
- `readOnly`可以是`boolean`，也可以是`函数`，接收2个参数，`第一个`参数是一行的数据，`第二个`参数是第几行
- 案例第二行,性别只读
:::demo
tableForm/index1
:::

## 同一列通过不同的规则，渲染不同的下拉框选项
- `optionsRule`是一个函数`函数`，接收2个参数，`第一个`参数是一行的数据，`第二个`参数是第几行
- 案例商户号
:::demo
tableForm/index2
:::

## AdTableForm Prop
| 属性名                  | 说明                    | 类型                                               | 默认值                | 属性         |
| ----------------------- | ----------------------- | -------------------------------------------------- | --------------------- | ------------- |
| `model-value / v-model` | 绑定值 | `Record<string, any>` | — | — |
| `table`                 | 表格配置                | `Partial<TableProps<T>>`                           | —                     | [查看](https://element-plus.org/zh-CN/component/table.html#table-%E5%B1%9E%E6%80%A7)             |
| `columns`               | 列配置                  | `Array<Partial<FormColumn<T>>>`                        | —                     | [查看](https://element-plus.org/zh-CN/component/table.html#table-column-%E5%B1%9E%E6%80%A7)             |
| `events`                | 表格事件配置            | `Partial<TableEmits<T>>`                           | —                     | [查看](https://element-plus.org/zh-CN/component/table.html#table-%E6%96%B9%E6%B3%95)             |
| `handleTableHeaderClick`| 表头点击回调            | <Attr content="(key: string, column: Partial<Column<T>>) => void"  type="func"/>| —                     | —             |
| `handleTableRowClick`   | 行点击回调              | <Attr content="(key: string, row: T, rowIndex: number) => void"  type="func"/>  | —                     | —             |
| `isAutoLoadable`        | 是否自动加载数据         | `boolean`                                          | true                     | —             |
| `api`                   | 数据获取 API            | <Attr content="(queryForm?: Record<string, any>) => Promise<any>"  type="func"/>| —               | —             |
| `reqHandler`            | 请求参数处理器          | <Attr content="(queryForm?: Record<string, any>) => Record<string, any>"  type="func"/> | —             | —             |
| `respHandler`           | 响应数据处理器          | <Attr content="(data?: Record<string, any>) => Record<string, any>"  type="func"/> | —                  | —             |
| `globalReadOnly`        | 全局只读                | `boolean`                                                                          | `false`                  | —             |
| `minQuantity`           | 限制最小数量            | `number`                                                                           | `1`                 | —             |
| `maxQuantity`           | 限制最大数量            | `number`                                                                           | —                  | —             |
| `showAdded`             | 是否展示新增按钮        | `boolean`                                                                          | `true`             | —             |
| `showDelete`            | 是否展示删除按钮        | `boolean`                                                                          | `true`             | —             |
| `submit`                | 接收表单验证成功的数据  | `(row: T[]) => void`                                                                          | `true`             | —             |


## 类型声明

::: details 显示类型声明

```ts
import type { TableColumnCtx, TableProps } from 'element-plus';
import { TableEmits } from '../table/types';
import { ComponentsProps } from '../form';
export type FormColumn<T = any> = TableColumnCtx<T> & {
  hidden: boolean;
  tag: boolean;
  readOnly: (row: T, index: number) => boolean | boolean;
} & ComponentsProps;

export interface AdTableFormPorps<M = any, T = any> {
  modelValue?: Record<string, any>;
  table?: Partial<TableProps<T>>;
  events?: Partial<TableEmits<T>>;
  globalReadOnly?: boolean;
  columns?: Array<Partial<FormColumn<T>>>;
  handleTableHeaderClick?: (key: string, column: Partial<FormColumn<T>>) => void;
  handleTableRowClick?: (key: string, row: T, rowIndex: number) => void;
  isAutoLoadable?: boolean;
  api: (queryForm?: Record<string, any>) => Promise<any>;
  reqHandler?: (queryForm?: M) => M;
  respHandler?: (data?: Record<string, any>) => Record<string, any>;
  minQuantity?: number;
  maxQuantity?: number;
  showAdded?: boolean;
  showDelete?: boolean;
  submit: (row: T[]) => void;
}

export interface AdTableFormEmits {}

export interface AdTableFormInstance {
  fetchData: () => Promise<void>;
  submit: () => void;
}

```
:::
