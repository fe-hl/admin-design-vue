---
title: message提示
description: message提示
---

# Message 消息提示

针对Message进行二次封装

## 不同状态的Message

:::demo
message/message1
:::

## 不同状态的MessageBox

- 默认title为`提示`,可以自定义title
- 默认confirmText为`确定`,可以自定义confirmText

:::demo
message/message2
:::


## confirm

- 默认title为`提示`,可以自定义title
- 默认confirmText为`确定`,可以自定义confirmText
- 默认cancelText为`取消`,可以自定义cancelText

:::demo
message/confirm
:::


## confirm延迟关闭

- 默认title为`提示`,可以自定义title
- 默认confirmText为`确定`,可以自定义confirmText
- 默认cancelText为`取消`,可以自定义cancelText

:::demo
message/confirmAsync
:::
