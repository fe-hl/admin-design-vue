const path = require('path');
const { readdirSync, statSync } = require('fs');
const { defineConfig, build } = require('vite');
const vue = require('@vitejs/plugin-vue');
const dts = require('vite-plugin-dts');
const vueJsx = require('@vitejs/plugin-vue-jsx');
const VueMacros = require('unplugin-vue-macros/vite');

const resolve = (dir) => path.join(__dirname, dir);

const baseoutDirPath = resolve('../');
const getFileEntrys = (filePath, list = []) => {
  readdirSync(filePath).forEach((filename) => {
    const filedir = path.join(filePath, filename);
    const fileStats = statSync(filedir);
    const isFile = fileStats.isFile();
    const isDir = fileStats.isDirectory();
    if (isDir) {
      // 目录递归
      getFileEntrys(filedir, list);
    }
    if (isFile && filedir.endsWith('index.ts')) {
      const paths = filedir.split(path.sep);
      const dirName = paths[paths.length - 2];
      list.push({
        dirName,
        filedir,
      });
    }
  });
  return list;
};

const generatorBuild = async ({ inputFiledir, fileName, moduleType, isDts }) => {
  const plugins = [
    VueMacros({
      plugins: {
        vue: vue(),
        vueJsx: vueJsx(),
      },
    }),
  ];
  // 类型声明，全量只打包一次
  if (isDts && fileName === 'index') {
    plugins.push(
      dts({
        outputDir: ['es', 'lib'],
        include: ['packages/**/*.*'],
        tsConfigFilePath: 'tsconfig.json',
        beforeWriteFile: (filePath, content) => {
          if (filePath.endsWith('components\\index\\index.d.ts')) {
            content = content.replace('../../hooks', './../hooks');
          }
          return {
            filePath: filePath.replace('components', '').replace('types.d.ts', 'index.d.ts'),
            content,
          };
        },
      }),
    );
  }
  await build(
    defineConfig({
      configFile: false,
      publicDir: false,
      css: {
        postcss: {
          plugins: [
            // 前缀追加
            // eslint-disable-next-line @typescript-eslint/no-var-requires
            require('autoprefixer')({
              overrideBrowserslist: ['Android 4.1', 'iOS 7.1', 'Chrome > 31', 'ff > 31', 'ie >= 8', '> 1%'],
              grid: true,
            }),
            require('postcss-flexbugs-fixes'),
          ],
        },
      },
      plugins: plugins,
      build: {
        target: 'modules',
        minify: 'esbuild',
        rollupOptions: {
          external: ['vue', 'elementPlus', 'element-plus', 'axios'],
          output: {
            globals: {
              vue: 'Vue',
            },
          },
        },
        lib: {
          entry: inputFiledir,
          name: moduleType === 'umd' ? 'AdAdmin' : '',
          fileName: (format) => {
            return `index.${format}.js`;
          },
          formats: [moduleType],
        },
        outDir: path.join(baseoutDirPath, moduleType === 'umd' ? 'lib' : 'es', fileName),
      },
    }),
  );
};

const componentPackaging = async (filedir, fileName) => {
  await generatorBuild({
    inputFiledir: filedir,
    fileName,
    moduleType: 'es',
    isDts: true,
  });
  await generatorBuild({
    inputFiledir: filedir,
    fileName,
    moduleType: 'umd',
    isDts: false,
  });
};

const init = async () => {
  let indexDir = {};
  const entrys = getFileEntrys(resolve('../packages/components')).filter((_) => {
    if (_.dirName === 'index') {
      indexDir = _;
      return false;
    }
    return true;
  });
  entrys.push(indexDir);
  for (const entry of entrys) {
    await componentPackaging(entry.filedir, entry.dirName);
  }
};
init();
