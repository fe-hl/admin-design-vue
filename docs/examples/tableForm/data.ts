export default {
  data: {
    records: [
      {
        name: '张三',
        age: '18',
        sex: '0',
        address: '北京市',
        status: '0',
        mchId: '10000',
        code: 'SH10000',
      },
      {
        name: '李四',
        age: '19',
        sex: '1',
        address: '北京市',
        status: '1',
        mchId: '20000',
        code: 'SH20000',
      },
    ],
  },
};
