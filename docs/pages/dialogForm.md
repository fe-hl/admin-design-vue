---
title: dialogForm | admin-design-vue
description: DialogForm 组件的文档
---

# DialogForm组件

通过配置JSON快速构建表单

## 快捷弹框表单配置

:::demo
dialogForm/index
:::

## 批量更新Models数据

:::demo
dialogForm/batch
:::

## DialogForm Prop
| 属性名        |     说明     |                类型 | 默认值 | 属性  |
| ------------- | :----------: | --------------------: | ---: | ------------- |
| `dialogProps` | element-plus DialogProps  | `DialogProps`  | — | [查看](https://element-plus.org/zh-CN/component/dialog.html#attributes) |
| `formPorps` | 表单配置属性 | `AdFormPorps` | — | [查看](https://fe-hl.gitee.io/admin-design-vue/document/pages/form.html#form-prop) |
| `events` | element-plus DialogEmits | — | — | [查看](https://element-plus.org/zh-CN/component/dialog.html#events) |


## DialogForm Exposes

| 名称        |     描述     |                类型 |
| ------------- | :----------: | --------------------: |
| `open` | 打开 Dialog | `AdDialogFormInstance` |


## DialogForm Slot

| 名称        |     描述     |                类型 |
| ------------- | :----------: | --------------------: |
| `default` | 内容 | — |

## 类型声明

::: details 显示类型声明

```ts
import type { DialogProps, DialogEmits } from 'element-plus';

import { AdFormPorps } from '../form';

export interface AdDialogFormPorps<T = any> {
  dialogProps?: Partial<DialogProps>;
  formPorps?: Partial<AdFormPorps<T>>;
  events?: Partial<DialogEmits>;
}

export interface AdDialogFormInstance {
  open: () => void;
}

```
:::
