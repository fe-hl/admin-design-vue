import DefaultTheme from 'vitepress/theme';
import { ElButton, ElTooltip, ElIcon, ElRow,ElUpload, ElTag } from 'element-plus';
import 'element-plus/theme-chalk/dark/css-vars.css';
import Demo from './../../components/Demo.vue';
import Enum from './../../components/Enum.vue';
import Attr from './../../components/Attr.vue'
import 'animate.css';
import 'element-plus/dist/index.css';
import './../../styles/index.scss';

import AdminDesign from "@fe-hl/admin-design-vue";
import "@fe-hl/admin-design-vue/es/index/style.css";
// import AdminDesign from "../../../es/index/index.es.js";
// import "../../../lib/index/style.css";

export default {
	...DefaultTheme,
	enhanceApp({ app }) {
		app.use(AdminDesign);
		app.component('Demo', Demo);
		app.component('Enum', Enum);
		app.component('Attr', Attr);
		app.component(ElButton.name, ElButton);
		app.component(ElTooltip.name, ElTooltip);
		app.component(ElIcon.name, ElIcon);
		app.component(ElRow.name, ElRow);
		app.component(ElUpload.name, ElUpload);
		app.component(ElTag.name, ElTag);
	},
};
