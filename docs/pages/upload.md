---
title: Upload | admin-design-vue
description: Upload 组件的文档
---

# Upload 文件上传

Upload进行二次封装，支持自定义上传，支持文件列表展示，支持上传进度展示，支持文件大小限制，支持文件类型限制，支持文件上传拦截

## 上传接口返回标准数据结构
- 如果数据不符合标准，需要通过自定义`respHandler`处理
```js
const res = {
  code: 0,
  msg: '上传成功',
  data: {
    name: 'plant-1.png',
    url: 'https://images/plant-1.png',
  },
};
```

## 基础用法

当上传成功的数量等于允许上传文件的最大数量，会自动关闭上传入口

:::demo
upload/index
:::

## 数据回显

可以支持操作上传、删除

:::demo
upload/index1
:::

## 数据预览

仅预览，不可操作上传、删除

:::demo
upload/index2
:::


## Upload Prop

| 属性名          | 说明            | 类型                              | 默认值   |
| --------------- | --------------- | --------------------------------- | -------- |
| `model-value / v-model` | 绑定值 | <Attr content="Array<Record<string, any>>"/> | — | — |
| `actionUrl`     | api地址        | `string`                          | —        |
| `respHandler`   | 响应数据处理器  | <Attr content="(data?: Record<string, any>) => Record<string, any>"  type="func"/> |
| `headers`       | 请求头信息      | <Attr content="Record<string, any>"/>         | —        |
| `accept`        | 接受上传的文件类型            | `string`                          | —        |
| `limit`         | 允许上传文件的最大数量         | `number`													| 1        |
| `sizeLimit`     | 允许上传文件的大小限制，单位为MB | `number`             | 20       |
| `preview`     	| 是否显示预览图      | `boolean`              | —        |
| `isTip`        	| 是否显示提示文本 		|  `boolean`        | —        |
| `tipText`       | 提示文本      			|  `string`                        | —        |


## 类型声明

::: details 显示类型声明

```ts
export interface AdUploadPorps<T = any> {
  actionUrl: string;
  respHandler?: (data?: Record<string, any>) => Record<string, any>;
  headers?: Record<string, any>;
  modelValue?: T;
  accept?: string;
  limit?: number;
  sizeLimit?: number;
  preview?: boolean;
  isTip?: boolean;
  tipText?: string;
}

export interface AdUploadEmits<T = any> {
  (event: 'update:modelValue', models: T): void;
}


```
:::
