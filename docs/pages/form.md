---
title: Form | admin-design-vue
description: Form 组件的文档
---

# Form组件

通过配置JSON快速构建表单

## 快捷搜索配置

:::demo
form/search
:::

## select-v2虚拟列表
- 适用于数据量庞大列表
:::demo
form/searchV2
:::

## select-options静态数据配置

:::demo
form/option1
:::

## select-options动态数据配置

:::demo
form/option2
:::

## select-options筛选选项配置

:::demo
form/option3
:::

## select-options远程搜索配置

:::demo
form/option4
:::

## select-options联动配置

:::demo
form/option5
:::


## select-options字段映射配置
options操作的数据是`value`和`label`，如果数据返回的不是`value`和`label`，可以通过`optionsLabelKey`、`optionsValueKey`配置自动转换

:::demo
form/option6
:::

## Form 表单

表单布局采用的是el-col，默认每个表单是宽度是24/6=4(一行可以排列4个表单)，可以手动指定`span`属性设置，例如`span="12"`

:::demo
form/data
:::


## Form Prop

| 属性名        |     说明     |                类型 | 默认值 | 属性  |
| ------------- | :----------: | --------------------: | ---: | ------------- |
| `model-value / v-model` | 绑定值 | <Attr content="Record<string, any>"/> | — | — |
| `models` | 表单数据回显 | <Attr content="Record<string, any>"/> | — | — |
| `formProps` | element-plus FormProps  | `FormProps`  | — | [查看](https://element-plus.org/zh-CN/component/form.html#form-attributes) |
| `gutter` | 栅格间隔 (同el-row) | `number` | 24 | — |
| `span` | 栅格占据的列数 (同el-col) | `number` | 6 | — |
| `components` | 组件列表 | <Attr content="ComponentsProps"  type="array"/>  | — | [查看](#componentsprops) |
| `buttonsPosition` | 操作按钮的位置 | <Enum types="start,center,end"/> | end | — |
| `buttonsSpan` | 栅格占据的列数 (同el-col) | `number` | 默认是`span`的值 | — |
| `hiddenSubmit` | 是否隐藏查询按钮 | `boolean` | false | — |
| `hiddenResetForm` | 是否重置取消按钮 | `boolean` |  false | — |
| `submitText` | 查询按钮文本 | `string` |  查询 | — |
| `resetFormText` | 重置按钮文本 | `string` |  重置 | — |
| `isResetFormApi` | 重置按钮是否触发提交 | `boolean` |  true | — |
| `submit` | 表单验证成功接口执行成功后触发 | <Attr content="(models: T) => Promise<void> \| void"  type="func"/> | — | — |
| `resetForm` | 重置按钮点击触发 | <Attr content="() => void"  type="func"/> | — | — |

## ComponentsProps

组件列表属性(components)

| 属性名        |     说明     |                类型 | 默认值 | 属性 |
| ------------- | :----------: | --------------------: | ---: | ------------- |
| `model` | 表单的key | `string` | — | — |
| `components` | 组件的类型 | <Enum types="el-input,el-select,el-date-picker,el-checkbox-group,el-radio-group,el-switch,el-input-number,el-rate,el-slider,el-tree-select"/> | — | — |
| `subComponents` | 组件的类型(select,checkbox,radio需要) | <Enum types="el-option,el-checkbox,el-radio"/> | — | — |
| `props` | 组件的属性(同组件的属性) | <Attr content="TypeComponentsProps"/> | — | [查看](#typeComponentsProps) |
| `events` | 组件事件(同组件的事件) | <Enum types="InputEmits, SelectEmits, DatePickerEmits,CheckboxGroupEmits,RadioGroupEmits,SwitchEmits,InputNumberEmits,RateEmits,SliderEmits,TreeSelecttEmits"/> | — | — |
| `options` | select,checkbox,radio | <Attr content="Array<SelectOptions> \| (() => Promise<Array<SelectOptions>>)" type="array"/> | — | [查看](#selectoptions) |
| `optionsLabelKey` | 映射options label | `string` | label | — |
| `optionsValueKey` | 映射options value | `string` | value | — |
| `stypeProps` | 自定义样式 | <Attr content="Record<string, any>"/> | — | — |

## TypeComponentsProps

每个组件(props)

| 属性名        |     说明     |                类型 | 默认值 | 属性 |
| ------------- | :----------: | --------------------: | ---: | ------------- |
| `label` | 表单项的名称 | `string` | — | — |
| `rules` | 表单项验证(同element-plus) | `Array<any>` | — | — |
| `span` | 表单项栅格占据的列数 | `number` | — | — |
| `hidden` | 是否隐藏表单项 | `boolean` | — | false |
| `defaultValue` | 表单项默认值 | `number` | — | — |
| `labelWidth` | 表单项名字长度 | `string` | — | — |
| `*` | 同element-plus表单项 | <Enum types="InputProps,SelectProps,SelectV2Props,DatePickerProps,CheckboxGroupProps,RadioGroupProps,SwitchProps,InputNumberProps,RateProps,SliderProps,TreeSelectProps"/> | — | — |

## SelectOptions

el-select、el-checkbox、el-radio(options)

| 属性名        |     说明     |                类型 | 默认值 | 属性 |
| ------------- | :----------: | --------------------: | ---: | ------------- |
| `label` | label |  <Enum types="string,number"/> | — | — |
| `value` | value |  <Enum types="string,number,boolean,object"/> | — | — |
| `disabled` | 是否禁用 |  `boolean` | false | — |
| `*` | 任意key值 |  `[key: string]: any` | — | — |

## Form Emits

| 名称        |     描述     |                类型 |
| ------------- | :----------: | --------------------: |
| `submitSucceed` | 表单提交重置后触发 | <Attr content="(event: 'submitSucceed'): void"  type="func"/> |


## Form Exposes

| 名称        |     描述     |                类型 |
| ------------- | :----------: | --------------------: |
| `ref` | 表单实例 | `FormInstance` |
| `loading` | 是否加载中 | `boolean` |

## Form Slot

| 名称        |     描述     |                类型 |
| ------------- | :----------: | --------------------: |
| `model` | model就是Slot名称 | `{scope:ComponentsProps}` |

## 类型声明

::: details 显示类型声明

```ts
import type {
  CheckboxGroupEmits,
  CheckboxGroupProps,
  DatePickerProps,
  FormInstance,
  FormProps,
  InputEmits,
  InputNumberEmits,
  InputNumberProps,
  InputProps,
  RadioGroupEmits,
  RadioGroupProps,
  RateEmits,
  RateProps,
  SliderEmits,
  SliderProps,
  SwitchEmits,
  SwitchProps,
	SelectContext,
	SelectV2Context
} from 'element-plus';

interface ExtendProps {
  label: string;
  rules: Array<any>;
  span: number;
  hidden: boolean;
  defaultValue: any;
  labelWidth?: string;
}

type SelectProps = SelectContext['props'];
type SelectV2Props = SelectV2Context['props'];

type TreeSelectProps = Record<string, any>;
type TypeComponentsProps =
  | (InputProps & ExtendProps)
  | (SelectProps & ExtendProps)
	| (SelectV2Props & ExtendProps)
  | (DatePickerProps & ExtendProps)
  | (CheckboxGroupProps & ExtendProps)
  | (RadioGroupProps & ExtendProps)
  | (SwitchProps & ExtendProps)
  | (InputNumberProps & ExtendProps)
  | (RateProps & ExtendProps)
  | (SliderProps & ExtendProps)
  | (TreeSelectProps & ExtendProps);

type SelectEmits = Record<string, any>;
type SelectV2Emits = Record<string, any>;
type TreeSelecttEmits = Record<string, any>;
type DatePickerEmits = Record<string, any>;
type ComponentsEvents =
  | InputEmits
  | SelectEmits
	| SelectV2Emits
  | DatePickerEmits
  | CheckboxGroupEmits
  | RadioGroupEmits
  | SwitchEmits
  | InputNumberEmits
  | RateEmits
  | SliderEmits
  | TreeSelecttEmits;
export interface OptionsGroups {
  [key: string]: {
    loading: boolean;
    list: Array<SelectOptions>;
  };
}

export interface SelectOptions {
  label?: string | number;
  value?: string | number | boolean | object;
  disabled?: boolean;
  [key: string]: any;
}

export interface ComponentsProps {
  model: string;
  components:
    | 'el-input'
    | 'el-select'
    | 'el-select-v2'
    | 'el-date-picker'
    | 'el-checkbox-group'
    | 'el-radio-group'
    | 'el-switch'
    | 'el-input-number'
    | 'el-rate'
    | 'el-slider'
    | 'el-tree-select';
  subComponents?: 'el-option' | 'el-checkbox' | 'el-radio';
  props?: Partial<TypeComponentsProps>;
  events?: Partial<ComponentsEvents>;
  dictKey?: string;
  options?: Array<SelectOptions> | ((keyword: string) => Promise<Array<SelectOptions>>);
  optionsLabelKey?: string;
  optionsValueKey?: string;
  stypeProps?: Record<string, any>;
}

export interface AdFormPorps<T = any> {
  modelValue?: T;
  models?: T;
  formProps?: Partial<FormProps>;
  gutter?: number;
  span?: number;
  components: Array<ComponentsProps>;
  buttonsPosition?: 'start' | 'center' | 'end';
  buttonsSpan?: number;
  hiddenSubmit?: boolean;
  hiddenResetForm?: boolean;
  submitText?: string;
  resetFormText?: string;
  isResetFormApi?: boolean;
  submit: (models: T) => Promise<void> | void;
  resetForm?: () => void;
}

export interface AdFormEmits<T = Record<string, any>> {
  (event: 'update:modelValue', models: T): void;
  (event: 'submitSucceed'): void;
}

export interface AdFormInstance {
  ref: FormInstance;
  loading: boolean;
}



```
:::
