import { withInstall } from './../../utils/index';

import _DialogForm from './DialogForm.vue';

export const DialogForm = withInstall(_DialogForm);

export default DialogForm;

export * from './types';

declare module 'vue' {
  export interface GlobalComponents {
    AdDialogForm: typeof DialogForm;
  }
}
