interface IMergeTableRows<T = any> {
  rowIndex: number;
  columnIndex: number;
  list: T[];
  rules: Record<string, number | Array<number>>;
}

/**
 * 获取合并行
 */
const getSpanArr = (dataList: any[], spanArr: Array<number>, fieldName: string) => {
  let pos: number = 0;
  for (let i = 0; i < dataList.length; i++) {
    if (i === 0) {
      spanArr.push(1);
      pos = 0;
    } else {
      let doesItHave = true;
      const fieldNames = fieldName.split('~');
      for (let index = 0; index < fieldNames.length; index++) {
        const field = fieldNames[index];
        if (dataList[i][field] !== dataList[i - 1][field]) {
          doesItHave = false;
          break;
        }
      }
      // 判断当前元素与上一个元素是否相同
      if (doesItHave) {
        spanArr[pos] += 1; //需要合并的行数
        spanArr.push(0); //新增被合并的行
      } else {
        spanArr.push(1);
        pos = i; //新的需要合并的第几行数
      }
    }
  }
  return spanArr;
};

// 缓存对象
const targetMap = new WeakMap();
export function useMergeTableRows<T = any>(payload: IMergeTableRows<T>) {
  const fieldNames: Array<string> = [];
  const colIndexArr: Array<number> = [];
  Object.keys(payload.rules).forEach((key) => {
    const v = payload.rules[key];
    if (Array.isArray(v)) {
      colIndexArr.push(...v);
      v.forEach(() => {
        fieldNames.push(key);
      });
    } else {
      fieldNames.push(key);
      colIndexArr.push(v);
    }
  });
  const list = fieldNames.map((fieldName) => {
    // 相同的ID减少计算的次数
    let depsMap = targetMap.get(payload.list);
    if (!depsMap) {
      targetMap.set(payload.list, (depsMap = new Map()));
    }
    let dep = depsMap.get(fieldName);
    if (!dep) {
      depsMap.set(fieldName, (dep = new Set()));
    } else {
      return dep.values().next().value;
    }
    const spanArr = getSpanArr(payload.list, [], fieldName);
    dep.add(spanArr);
    return spanArr;
  });
  const newList: Array<any> = [];
  let i = 0;
  colIndexArr.forEach((colIndex) => {
    newList[colIndex] = list[i++];
  });
  if (colIndexArr.indexOf(payload.columnIndex) !== -1) {
    const _row = newList[payload.columnIndex][payload.rowIndex];
    const _col = _row > 0 ? 1 : 0;
    return { rowspan: _row, colspan: _col };
  }
}
