import { withInstall } from './../../utils/index';

import _Form from './Form.vue';
import { ComponentsEvents } from './types';

export const Form = withInstall(_Form);

export default Form;

export * from './types';
// change 事件增强
export const eventsBind = (events?: ComponentsEvents) => {
  if (!events) {
    return {};
  }
  const v = { ...events };
  delete v.change;
  return v;
};

export const changeAction = (
  value: Array<string | number> | string | number,
  model: Record<string, any>,
  index: number,
  options: Array<Record<string, any>>,
  optionsValueKey: string,
  change: (model: Record<string, any>, data: Record<string, any>, value: Array<string | number> | string | number, index: number) => void,
) => {
  const data = options.find((_) => _[optionsValueKey] == value) || {};
  change(model, data, value, index);
};

declare module 'vue' {
  export interface GlobalComponents {
    AdForm: typeof Form;
  }
}
