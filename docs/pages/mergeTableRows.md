---
title: 表格自动行合并
description: 表格自动行合并
---

# 表格自动行合并

针对Table表格行合并封装,支持缓存减少大量的重复计算

## 根据一个字段单列合并

:::demo
mergeTableRows/table1
:::

## 根据多个字段多列合并

:::demo
mergeTableRows/table2
:::


## 根据一个字段多列合并

:::demo
mergeTableRows/table3
:::

## 根据多个字段组合合并

:::demo
mergeTableRows/table5
:::



## 结合AdTable组件合并

:::demo
mergeTableRows/table4
:::

## Func参数

| 名称        |     描述     |                类型 |
| ------------- | :----------: | --------------------: |
| `rowIndex` | 行 | `number` |
| `columnIndex` | 列 | `number` |
| `list` | 数据源 | `T[]` |
| `rules` | 合并规则 | <Attr content="Record<string, number \| Array<number>>" /> |


## 类型声明

::: details 显示类型声明

```ts
interface IMergeTableRows<T = any> {
  rowIndex: number; // 行
  columnIndex: number; // 列
  list: T[]; // 列表数据
  rules: Record<string, number | Array<number>>; // 合并规则 {id:0} OR {id:[2,4]} OR {'id~name': [0, 1]}
}


```
:::
