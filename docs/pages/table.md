---
title: AdTable | admin-design-vue
description: AdTable 组件的文档
---

# AdTable组件

通过配置JSON快速构建列表、分页、搜索、导出、自定义列、自定义排序等功能。

## 按钮权限控制

[如何设置权限数据](https://fe-hl.github.io/document/pages/permissionDirective.html)


## 标准的列表的数据结构

```js
const res = {
  data: {
    records: [],
    total: 0,
  },
};
```

## 快捷列表配置

:::demo
table/index
:::


## 请求响应数据格式化

如果返回的数据不是标准的格式，可以通过`respHandler`进行格式化

请求的数据可以通过`reqHandler`进行格式化
:::demo
table/table1
:::


## headerAction按钮禁用控制

- effect默认没有控制按钮
- effect配置是`one`时，单个选中才能启用
- effect配置是`more`时，只有有选择就启用

:::demo
table/table2
:::

## 插槽的运用

插槽名就是`prop`字段对应的的key


:::demo
table/table3
:::

## AdTable Prop

| 属性名                  | 说明                    | 类型                                               | 默认值                | 属性         |
| ----------------------- | ----------------------- | -------------------------------------------------- | --------------------- | ------------- |
| `model-value / v-model` | 绑定值 | `Record<string, any>` | — | — |
| `headerAction`          | 表头操作按钮            | `Array<Action<T>>`                                 | —                     | —             |
| `columnAction`          | 列操作按钮              | `Array<Action<T>>`                                 | —                     | —             |
| `columnActionLabel`     | 列操作按钮标签          | `string`                                           | —                     | —             |
| `columnActionWidth`     | 列操作按钮宽度          | `number \| string`                                 | —                     | —             |
| `actionClick`           | 操作按钮点击回调        | <Attr content="(action: Action<T>, rows: Array<T>) => void"  type="func"/>      | —                     | —             |
| `table`                 | 表格配置                | `Partial<TableProps<T>>`                           | —                     | [查看](https://element-plus.org/zh-CN/component/table.html#table-%E5%B1%9E%E6%80%A7)             |
| `events`                | 表格事件配置            | `Partial<TableEmits<T>>`                           | —                     | [查看](https://element-plus.org/zh-CN/component/table.html#table-%E6%96%B9%E6%B3%95)             |
| `columns`               | 列配置                  | `Array<Partial<Column<T>>>`                        | —                     | [查看](https://element-plus.org/zh-CN/component/table.html#table-column-%E5%B1%9E%E6%80%A7)             |
| `doubleClickPickOn`     | 双击行是否选中          | `boolean`                                          | —                     | —             |
| `handleTableHeaderClick`| 表头点击回调            | <Attr content="(key: string, column: Partial<Column<T>>) => void"  type="func"/>| —                     | —             |
| `handleTableRowClick`   | 行点击回调              | <Attr content="(key: string, row: T, rowIndex: number) => void"  type="func"/>  | —                     | —             |
| `isAutoLoadable`        | 是否自动加载数据         | `boolean`                                          | true                     | —             |
| `api`                   | 数据获取 API            | <Attr content="(queryForm?: Record<string, any>) => Promise<any>"  type="func"/>| —               | —             |
| `exportExcelApi`        | 导出Excel数据 API        | <Attr content="(queryForm?: Record<string, any>) => void"  type="func"/>        | —                     | —             |
| `reqHandler`            | 请求参数处理器          | <Attr content="(queryForm?: Record<string, any>) => Record<string, any>"  type="func"/> | —             | —             |
| `respHandler`           | 响应数据处理器          | <Attr content="(data?: Record<string, any>) => Record<string, any>"  type="func"/> | —                  | —             |
| `toolbarSearchText`     | 搜索按钮文本            | `string`                                           | 隐藏搜索                     | —             |
| `toolbarSearchHiddenText`| 隐藏搜索按钮文本        | `string`                                           | 显示搜索                     | —             |
| `toolbarDownloadText`   | 下载按钮文本            | `string`                                           | 导出                     | —             |
| `toolbarRefreshText`    | 刷新按钮文本            | `string`                                           | 刷新                     | —             |
| `toolbarColumnsText`    | 列显示/隐藏按钮文本     | `string`                                           | 显隐列                     | —             |
| `toolbarSortText`       | 排序按钮文本            | `string`                                           | 排序                     | —             |
| `toolbarDownloadPerm` | 下载权限配置         | `Array<string> \| string`                          | —                     | —             |
| `isToolbarDownload`     | 是否显示下载按钮        | `boolean`                                          | true                     | —             |
| `isToolbarColumns`      | 是否显示列按钮          | `boolean`                                          | true                     | —             |
| `isToolbarSort`         | 是否显示排序按钮        | `boolean`                                          | true                     | —             |
| `isToolbar`             | 是否显示工具栏          | `boolean`                                          | —                     | —             |
| `isPaging`              | 是否启用分页            | `boolean`                                          | true                     | —             |
| `pageSize`              | 每页显示条目数          | `number`                                           | 10                     | —             |
| `pageSizes`             | 每页显示条目数选项      | `Array<number>`                                    | [10, 20, 30, 40, 50]                     | —             |

## Action Prop

| 属性名          | 说明            | 类型                              | 默认值   |
| --------------- | --------------- | --------------------------------- | -------- |
| `actionType`    | 操作类型        | `string`                          | —        |
| `actionName`    | 操作名称        | `string`                          | —        |
| `perm`          | 权限配置        | `string \| Array<string>`         | —        |
| `icon`          | 图标            | `string`                          | —        |
| `color`         | 颜色            | <Enum types="primary,success,info,warning,danger"/>| primary        |
| `hidden`        | 是否隐藏        | `(row: T) => boolean`             | —        |
| `formatter`     | 格式化函数      | `(row: T) => string`              | —        |
| `effect`        | 操作按钮禁用      |  <Enum types="one,more"/>        | —        |


## AdTable Exposes

| 名称        |     描述     |                类型 |
| ------------- | :----------: | --------------------: |
| `fetchData` | 刷新列表 | `AdTableInstance` |
| `list` | 列表数据 | `AdTableInstance` |

## AdTable Slot

| 名称        |     描述     |                类型 |
| ------------- | :----------: | --------------------: |
| `default` | 表单组件 | — |
| `total` | 汇总数据 | — |

## 类型声明

::: details 显示类型声明

```ts
import type { TableProps, TableColumnCtx } from 'element-plus';

export interface Action<T = any> {
  actionType: string;
  actionName: string;
  perm?: string | Array<string>;
  icon?: string;
  color?: 'primary' | 'success' | 'info' | 'warning' | 'danger';
  hidden?: (row: T) => boolean;
  formatter?: (row: T) => string;
  effect?: 'one' | 'more';
}

interface TableEmits<T = any> {
  select: (selection: T[], row: T) => void;
  selectAll: (selection: T[]) => void;
  selectionChange: (selection: T[]) => void;
  cellMouseEnter: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  cellMouseLeave: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  cellClick: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  cellDblclick: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  cellContextmenu: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  rowClick: (row: T, column: TableColumnCtx<T>, event: Event) => void;
  rowContextmenu: (row: T, column: TableColumnCtx<T>, event: Event) => void;
  rowDblclick: (row: T, column: TableColumnCtx<T>, event: Event) => void;
  headerClick: (column: TableColumnCtx<T>, event: Event) => void;
  headerContextmenu: (column: TableColumnCtx<T>, event: Event) => void;
  sortChange: (sortData: { column: TableColumnCtx<T>; prop: string; order: string }) => void;
  filterChange: (filterData: { filters: any[]; columnKey: string }) => void;
  currentChange: (currentrow: T, oldCurrentrow: T) => void;
  headerDragend: (dragData: { newWidth: number; oldWidth: number; column: any; event: Event }) => void;
  expandChange: (row: T, expandedRows: any[] | boolean) => void;
}

export type Column<T = any> = TableColumnCtx<T> & { hidden: boolean, tag:boolean };

export interface AdTablePorps<M = any, T = any> {
  modelValue?: Record<string, any>;
  headerAction?: Array<Action<T>>;
  columnAction?: Array<Action<T>>;
  columnActionLabel?: string;
  columnActionWidth?: number | string;
  actionClick?: (action: Action<T>, rows: Array<T>) => void;
  table?: Partial<TableProps<T>>;
  events?: Partial<TableEmits<T>>;
  columns?: Array<Partial<Column<T>>>;
  doubleClickPickOn?: boolean;
  handleTableHeaderClick?: (key: string, column: Partial<Column<T>>) => void;
  handleTableRowClick?: (key: string, row: T, rowIndex: number) => void;
  isAutoLoadable?: boolean;
  api: (queryForm?: Record<string, any>) => Promise<any>;
  exportExcelApi?: (queryForm?: Record<string, any>) => void;
  reqHandler?: (queryForm?: M) => M;
  respHandler?: (data?: Record<string, any>) => Record<string, any>;
  toolbarSearchText?: string;
  toolbarSearchHiddenText?: string;
  toolbarDownloadText?: string;
  toolbarRefreshText?: string;
  toolbarColumnsText?: string;
  toolbarSortText?: string;
  toolbarDownloadPerm?: Array<string> | string;
  isToolbarDownload?: boolean;
  isToolbarColumns?: boolean;
  isToolbarSort?: boolean;
  isToolbar?: boolean;
  isPaging?: boolean;
  pageSize?: number;
  pageSizes?: Array<number>;
}

export interface AdTableEmits {}

export interface AdTableInstance {
  fetchData: (params?: { isResetPageNumber: boolean }) => Promise<void>;
	list: Array<any>;
}


```
:::

