import type { App } from 'vue';
import {
  ElForm,
  ElFormItem,
  ElRow,
  ElCol,
  ElButton,
  ElInput,
  ElSelect,
  ElSelectV2,
  ElDatePicker,
  ElCheckboxGroup,
  ElCheckbox,
  ElRadioGroup,
  ElRadio,
  ElOption,
  ElSwitch,
  ElInputNumber,
  ElSlider,
  ElTreeSelect,
  ElRate,
  ElDialog,
  ElTable,
  ElPagination,
  ElTableColumn,
  ElTooltip,
  ElDropdown,
  ElDropdownItem,
  ElDropdownMenu,
} from 'element-plus';
import { Form } from './../form';
import { DialogForm } from '../dialogForm';
import { Table } from '../table';
import { TableForm } from '../tableForm';
import { Upload } from '../upload';
import { hasPermi, hasRole } from './permission';

export * from './../form';
export * from './../dialogForm';
export * from './../table';
export * from './../tableForm';
export * from './../upload';
export * from '../../hooks';

const components = [
  ElForm,
  ElFormItem,
  ElRow,
  ElCol,
  ElInput,
  ElSelect,
  ElSelectV2,
  ElOption,
  ElDatePicker,
  ElCheckboxGroup,
  ElCheckbox,
  ElRadioGroup,
  ElRadio,
  ElSwitch,
  ElButton,
  ElInputNumber,
  ElRate,
  ElSlider,
  ElTreeSelect,
  ElDialog,
  ElTable,
  ElTableColumn,
  ElPagination,
  ElTooltip,
  ElDropdown,
  ElDropdownItem,
  ElDropdownMenu,
  Form,
  DialogForm,
  Table,
  TableForm,
  Upload,
];

const install: any = function (Vue: App) {
  if (install.installed) return;
  install.installed = true;

  components.forEach((component) => {
    Vue.use(component);
  });
  Vue.directive('hasRole', hasRole);
  Vue.directive('hasPermi', hasPermi);
};

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

/**
 *
 * @param role 角色
 * @param permi 权限
 * @param storage 存储方式
 */

export const setPermission = (role: Array<string>, permi: Array<string>, storage: 'localStorage' | 'sessionStorage') => {
  window[storage].setItem('AD_ROLE', JSON.stringify(role));
  window[storage].setItem('AD_PERMI', JSON.stringify(permi));
  window.permi = storage;
};

export default install;
