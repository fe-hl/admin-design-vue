import { Component, createApp, h } from 'vue';
function mountComponent(RootComponent: Component) {
  const app = createApp(RootComponent);
  const root = document.createElement('div');
  document.body.appendChild(root);
  return {
    instance: app.mount(root),
    unmount() {
      app.unmount();
      document.body.removeChild(root);
    },
  };
}

export function createInstance(component: Component) {
  return (props: any = {}) => {
    const { unmount } = mountComponent({
      setup() {
        props.close = () => {
          unmount();
        };
        return () => {
          return h(component, props);
        };
      },
    });
  };
}
