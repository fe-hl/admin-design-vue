---
title: dialogForm | admin-design-vue
description: 权限指令
---

# 权限指令

配合`v-hasPermi`、`v-hasRole`指令使用，可以实现对按钮的权限控制。

## 设置权限数据

:::tip
权限数据必须需要设置，否则无效。
:::

```ts
import { setPermission } from '@fe-hl/admin-design-vue';
/**
 *
 * @param role 角色
 * @param permi 权限
 * @param storage 存储方式 'localStorage' | 'sessionStorage'
 */

setPermission(['admin1','admin2'],['add','del'],'localStorage')
```


## 角色指令

```ts
<el-button v-hasRole="admin">新增</el-button>
<el-button v-hasRole="['admin','audit']">审核</el-button>
```


## 权限指令

```ts
<el-button v-hasPermi="admin">新增</el-button>
<el-button v-hasPermi="['admin','audit']">审核</el-button>
```





