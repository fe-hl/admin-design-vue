# 快速开始

本节将介绍如何在项目中使用 @fe-hl/admin-design-vue。

## 用法

### 完整引入

如果你对打包后的文件大小不是很在乎，那么使用完整导入会更方便.

```typescript
// main.ts
import { createApp } from 'vue';
import AdminDesign from '@fe-hl/admin-design-vue';
import '@fe-hl/admin-design-vue/lib/index/style.css';
import App from './App.vue';

const app = createApp(App);

app.use(AdminDesign);
app.mount('#app');
```

