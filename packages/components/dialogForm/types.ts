import type { DialogProps, DialogEmits } from 'element-plus';

import { AdFormPorps } from '../form';

export interface AdDialogFormPorps<T = any> {
  dialogProps?: Partial<DialogProps>;
  formPorps?: Partial<AdFormPorps<T>>;
  events?: Partial<DialogEmits>;
}

export interface AdDialogFormInstance {
  open: () => void;
  updateModels: (models: Record<string, any> | null) => void;
}
