export interface AdUploadPorps<T = any> {
  actionUrl: string;
  respHandler?: (data?: Record<string, any>) => Record<string, any>;
  headers?: Record<string, any>;
  modelValue?: T;
  accept?: string;
  limit?: number;
  sizeLimit?: number;
  preview?: boolean;
  isTip?: boolean;
  tipText?: string;
}

export interface AdUploadEmits<T = any> {
  (event: 'update:modelValue', models: T): void;
}
