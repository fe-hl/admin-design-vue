import { ElMessage, ElMessageBox, MessageBoxData } from 'element-plus';
import { VNode } from 'vue';

interface IUseMessage {
  info(message: string | VNode): void;
  success(message: string | VNode): void;
  warning(message: string | VNode): void;
  error(message: string | VNode): void;
}

export function useMessage(): IUseMessage {
  class MessageClass {
    info(message: string | VNode): void {
      ElMessage.info(message);
    }

    success(message: string | VNode): void {
      ElMessage.success(message);
    }
    warning(message: string | VNode): void {
      ElMessage.warning(message);
    }

    error(message: string | VNode): void {
      ElMessage.error(message);
    }
  }

  return new MessageClass();
}

interface IUseMessageBox {
  info(message: string, confirmText?: string, title?: string): void;
  success(message: string, confirmText?: string, title?: string): void;
  warning(message: string, confirmText?: string, title?: string): void;
  error(message: string, confirmText?: string, title?: string): void;
  confirm(message: string, title?: string, confirmText?: string, cancelText?: string): Promise<MessageBoxData>;
  confirmAsync(
    asyncHandler: () => Promise<void>,
    message: string,
    title?: string,
    confirmText?: string,
    cancelText?: string,
  ): Promise<MessageBoxData>;
}

export function useMessageBox(): IUseMessageBox {
  class MessageBoxClass {
    info(message: string, confirmText?: string, title?: string): void {
      ElMessageBox.alert(message, title || '提示', {
        confirmButtonText: confirmText || '确认',
      });
    }

    success(message: string, confirmText?: string, title?: string): void {
      ElMessageBox.alert(message, title || '提示', { type: 'success', confirmButtonText: confirmText || '确认' });
    }

    warning(message: string, confirmText?: string, title?: string): void {
      ElMessageBox.alert(message, title || '提示', { type: 'warning', confirmButtonText: confirmText || '确认' });
    }

    error(message: string, confirmText?: string, title?: string): void {
      ElMessageBox.alert(message, title || '提示', { type: 'error', confirmButtonText: confirmText || '确认' });
    }

    confirm(message: string, title?: string, confirmText?: string, cancelText?: string) {
      return ElMessageBox.confirm(message, title || '提示', {
        confirmButtonText: confirmText || '确认',
        cancelButtonText: cancelText || '取消',
        type: 'warning',
      });
    }
    confirmAsync(asyncHandler: () => Promise<void>, message: string, title?: string, confirmText?: string, cancelText?: string) {
      return ElMessageBox.confirm(message, title || '提示', {
        confirmButtonText: confirmText || '确认',
        cancelButtonText: cancelText || '取消',
        type: 'warning',
        beforeClose: async (action: string, instance: { confirmButtonLoading: boolean; confirmButtonText: string }, done: () => void) => {
          if (action === 'confirm') {
            try {
              instance.confirmButtonLoading = true;
              instance.confirmButtonText = 'Loading...';
              await asyncHandler();
              done();
              setTimeout(() => {
                instance.confirmButtonLoading = false;
              }, 300);
            } catch (error) {
              instance.confirmButtonText = confirmText || '确认';
              setTimeout(() => {
                instance.confirmButtonLoading = false;
              }, 300);
            }
          } else {
            if (instance.confirmButtonLoading) return;
            done();
          }
        },
      });
    }
  }

  return new MessageBoxClass();
}
