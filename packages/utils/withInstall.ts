import type { App, Component } from 'vue';
import { createInstance } from './mountComponent';

export type WithInstall<T> = T & {
  install(app: App): void;
};

export function withInstall<T extends Component>(options: T, isPlugins: boolean = false) {
  (options as Record<string, unknown>).install = (app: App) => {
    const { name } = options;
    app.component(name!, options);
    if (isPlugins) {
      app.config.globalProperties['$' + name] = createInstance(options);
    }
  };

  return options as WithInstall<T>;
}
