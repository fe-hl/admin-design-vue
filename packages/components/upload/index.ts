import { withInstall } from './../../utils/index';

import _Upload from './Upload.vue';

export const Upload = withInstall(_Upload);

export default Upload;

export * from './types';

declare module 'vue' {
  export interface GlobalComponents {
    AdUpload: typeof Upload;
  }
}
