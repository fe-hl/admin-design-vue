import { defineConfig } from 'vitepress';
import path from 'path';
import { mdPlugin } from './../plugins/markdownIt';
const resolve = dir => path.join(__dirname, dir);
const IS_PROD = () => {
	return process.argv[process.argv.length - 1] === 'build';
};
export default defineConfig({
	outDir: resolve('../../document'),
	base: IS_PROD() ? '/document/' : '/',
	title: '组件文档',
	description: '组件文档',
	head: [
		['link', { rel: 'icon', type: 'image/svg+xml', href: 'images/logo.svg' }],
	],
	lastUpdated: true,
	markdown: {
		theme: 'material-palenight',
		lineNumbers: false,
		config: md => mdPlugin(md),
	},
	themeConfig: {
		lastUpdatedText: '上次更新',
		editLink: {
			pattern: 'https://gitee.com/fe-hl/admin-design-vue',
			text: '在GitHub上编辑此页',
		},
		logo: 'images/logo.svg',
		nav: [
			{ text: '主页', link: '/' },
			{ text: '指南', link: '/pages/install' },
			{ text: '更新日志', link: '/pages/updateLog' },
		],

		socialLinks: [
			{
				icon: 'github',
				link: 'https://gitee.com/fe-hl/admin-design-vue',
			},
		],
		sidebar: [
			{
				text: '开发指南',
				collapsible: true,
				items: [
					{
						text: '安装',
						link: '/pages/install',
					},
					{
						text: '快速开始',
						link: '/pages/quickstart',
					},
				],
			},
			{
				text: '业务组件',
				collapsible: true,
				items: [
					{
						text: 'Form 表单',
						link: '/pages/form',
					},
					{
						text: 'DialogForm 弹框表单',
						link: '/pages/dialogForm',
					},
					{
						text: 'Table 表格',
						link: '/pages/table',
					},
					{
						text: 'TableForm 表格表单',
						link: '/pages/tableForm',
					},
					{
						text: 'Upload 文件上传',
						link: '/pages/upload',
					},
					{
						text: 'permission 权限指令',
						link: '/pages/permissionDirective',
					},
				],
			},
			{
				text: 'hooks',
				collapsible: true,
				items: [
					{
						text: 'Message 消息提示',
						link: '/pages/message',
					},
					{
						text: 'MergeTableRows 表格自动行合并',
						link: '/pages/mergeTableRows',
					},
				],
			},
		],
	},
});
