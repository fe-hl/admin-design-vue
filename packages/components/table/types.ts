import type { TableProps, TableColumnCtx } from 'element-plus';

export interface Action<T = any> {
  actionType: string;
  actionName: string;
  perm?: string | Array<string>;
  icon?: string;
  color?: 'primary' | 'success' | 'info' | 'warning' | 'danger';
  hidden?: (row: T) => boolean;
  formatter?: (row: T) => string;
  effect?: 'one' | 'more';
}

export interface TableEmits<T = any> {
  select: (selection: T[], row: T) => void;
  selectAll: (selection: T[]) => void;
  selectionChange: (selection: T[]) => void;
  cellMouseEnter: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  cellMouseLeave: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  cellClick: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  cellDblclick: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  cellContextmenu: (row: T, column: TableColumnCtx<T>, cell: any, event: Event) => void;
  rowClick: (row: T, column: TableColumnCtx<T>, event: Event) => void;
  rowContextmenu: (row: T, column: TableColumnCtx<T>, event: Event) => void;
  rowDblclick: (row: T, column: TableColumnCtx<T>, event: Event) => void;
  headerClick: (column: TableColumnCtx<T>, event: Event) => void;
  headerContextmenu: (column: TableColumnCtx<T>, event: Event) => void;
  sortChange: (sortData: { column: TableColumnCtx<T>; prop: string; order: string }) => void;
  filterChange: (filterData: { filters: any[]; columnKey: string }) => void;
  currentChange: (currentrow: T, oldCurrentrow: T) => void;
  headerDragend: (dragData: { newWidth: number; oldWidth: number; column: any; event: Event }) => void;
  expandChange: (row: T, expandedRows: any[] | boolean) => void;
}

export type Column<T = any> = TableColumnCtx<T> & { hidden: boolean; tag: boolean };

export interface AdTablePorps<M = any, T = any> {
  modelValue?: Record<string, any>;
  headerAction?: Array<Action<T>>;
  columnAction?: Array<Action<T>>;
  columnActionLabel?: string;
  columnActionWidth?: number | string;
  actionClick?: (action: Action<T>, rows: Array<T>) => void;
  table?: Partial<TableProps<T>>;
  events?: Partial<TableEmits<T>>;
  columns?: Array<Partial<Column<T>>>;
  doubleClickPickOn?: boolean;
  handleTableHeaderClick?: (key: string, column: Partial<Column<T>>) => void;
  handleTableRowClick?: (key: string, row: T, rowIndex: number) => void;
  isAutoLoadable?: boolean;
  api: (queryForm?: Record<string, any>) => Promise<any>;
  exportExcelApi?: (queryForm?: Record<string, any>) => void;
  reqHandler?: (queryForm?: M) => M;
  respHandler?: (data?: Record<string, any>) => Record<string, any>;
  toolbarSearchText?: string;
  toolbarSearchHiddenText?: string;
  toolbarDownloadText?: string;
  toolbarRefreshText?: string;
  toolbarColumnsText?: string;
  toolbarSortText?: string;
  toolbarDownloadPerm?: Array<string> | string;
  isToolbarDownload?: boolean;
  isToolbarColumns?: boolean;
  isToolbarSort?: boolean;
  isToolbar?: boolean;
  isPaging?: boolean;
  pageSize?: number;
  pageSizes?: Array<number>;
}

export interface AdTableEmits {}

export interface AdTableInstance {
  fetchData: (params?: { isResetPageNumber: boolean }) => Promise<void>;
  list: Array<any>;
}
