import { withInstall } from './../../utils/index';

import _TableForm from './TableForm.vue';

export const TableForm = withInstall(_TableForm);

export default TableForm;

export * from './types';

declare module 'vue' {
  export interface GlobalComponents {
    AdTableForm: typeof TableForm;
  }
}
