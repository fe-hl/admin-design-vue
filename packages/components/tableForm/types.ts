import type { TableColumnCtx, TableProps } from 'element-plus';
import { TableEmits } from '../table/types';
import { ComponentsProps, SelectOptions } from '../form';
export type FormColumn<T = any> = TableColumnCtx<T> & {
  hidden: boolean;
  tag: boolean;
  readOnly: (row: T, index: number) => boolean | boolean;
  optionsRule: (row: Record<string, any>, index: number) => Array<SelectOptions>;
} & ComponentsProps;

export interface AdTableFormPorps<M = any, T = any> {
  modelValue?: Record<string, any>;
  table?: Partial<TableProps<T>>;
  events?: Partial<TableEmits<T>>;
  globalReadOnly?: boolean;
  columns?: Array<Partial<FormColumn<T>>>;
  handleTableHeaderClick?: (key: string, column: Partial<FormColumn<T>>) => void;
  handleTableRowClick?: (key: string, row: T, rowIndex: number) => void;
  isAutoLoadable?: boolean;
  api: (queryForm?: Record<string, any>) => Promise<any>;
  reqHandler?: (queryForm?: M) => M;
  respHandler?: (data?: Record<string, any>) => Record<string, any>;
  minQuantity?: number;
  maxQuantity?: number;
  showAdded?: boolean;
  showDelete?: boolean;
  submit: (row: T[]) => void;
}

export interface AdTableFormEmits {}

export interface AdTableFormInstance {
  fetchData: () => Promise<void>;
  submit: () => void;
}
