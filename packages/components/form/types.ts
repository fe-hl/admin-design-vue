import type {
  CheckboxGroupEmits,
  CheckboxGroupProps,
  DatePickerProps,
  FormInstance,
  FormProps,
  InputEmits,
  InputNumberEmits,
  InputNumberProps,
  InputProps,
  RadioGroupEmits,
  RadioGroupProps,
  RateEmits,
  RateProps,
  SliderEmits,
  SliderProps,
  SwitchEmits,
  SwitchProps,
  SelectContext,
  SelectV2Context,
} from 'element-plus';

interface ExtendProps {
  label: string;
  rules: Array<any>;
  span: number;
  hidden: boolean;
  defaultValue: any;
  labelWidth?: string;
}

type SelectProps = SelectContext['props'];
type SelectV2Props = SelectV2Context['props'];

type TreeSelectProps = Record<string, any>;
type TypeComponentsProps =
  | (InputProps & ExtendProps)
  | (SelectProps & ExtendProps)
  | (SelectV2Props & ExtendProps)
  | (DatePickerProps & ExtendProps)
  | (CheckboxGroupProps & ExtendProps)
  | (RadioGroupProps & ExtendProps)
  | (SwitchProps & ExtendProps)
  | (InputNumberProps & ExtendProps)
  | (RateProps & ExtendProps)
  | (SliderProps & ExtendProps)
  | (TreeSelectProps & ExtendProps);

type SelectEmits = Record<string, any>;
type SelectV2Emits = Record<string, any>;
type TreeSelecttEmits = Record<string, any>;
type DatePickerEmits = Record<string, any>;
export type ComponentsEvents =
  | InputEmits
  | SelectEmits
  | SelectV2Emits
  | DatePickerEmits
  | CheckboxGroupEmits
  | RadioGroupEmits
  | SwitchEmits
  | InputNumberEmits
  | RateEmits
  | SliderEmits
  | TreeSelecttEmits;
export interface OptionsGroups {
  [key: string]: {
    loading: boolean;
    list: Array<SelectOptions>;
  };
}

export interface SelectOptions {
  label?: string | number;
  value?: string | number | boolean | object;
  disabled?: boolean;
  [key: string]: any;
}

export interface ComponentsProps {
  model: string;
  components:
    | 'el-input'
    | 'el-select'
    | 'el-select-v2'
    | 'el-date-picker'
    | 'el-checkbox-group'
    | 'el-radio-group'
    | 'el-switch'
    | 'el-input-number'
    | 'el-rate'
    | 'el-slider'
    | 'el-tree-select';
  subComponents?: 'el-option' | 'el-checkbox' | 'el-radio';
  props?: Partial<TypeComponentsProps>;
  events?: Partial<ComponentsEvents>;
  dictKey?: string;
  options?: Array<SelectOptions> | ((keyword: string) => Promise<Array<SelectOptions>>);
  optionsLabelKey?: string;
  optionsValueKey?: string;
  stypeProps?: Record<string, any>;
}

export interface AdFormPorps<T = any> {
  modelValue?: T;
  models?: T;
  formProps?: Partial<FormProps>;
  gutter?: number;
  span?: number;
  components: Array<ComponentsProps>;
  buttonsPosition?: 'start' | 'center' | 'end';
  buttonsSpan?: number;
  hiddenSubmit?: boolean;
  hiddenResetForm?: boolean;
  submitText?: string;
  resetFormText?: string;
  isResetFormApi?: boolean;
  submit: (models: T) => Promise<void> | void;
  resetForm?: () => void;
}

export interface AdFormEmits<T = Record<string, any>> {
  (event: 'update:modelValue', models: T): void;
  (event: 'submitSucceed'): void;
}

export interface AdFormInstance {
  ref: FormInstance;
  loading: boolean;
}
