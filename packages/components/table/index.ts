import { withInstall } from './../../utils/index';

import _Table from './Table.vue';

export const Table = withInstall(_Table);

export default Table;

export * from './types';

declare module 'vue' {
  export interface GlobalComponents {
    AdTable: typeof Table;
  }
}
