function getPermi(permiType: string) {
  if (window.permi) {
    const permi = window[window.permi].getItem(permiType);
    return permi ? JSON.parse(permi) : [];
  }
  return [];
}

/**
 * v-hasPermi 操作权限处理
 */

export const hasPermi = {
  mounted(el: HTMLElement, binding: { value: Array<string> | string }) {
    let { value } = binding;
    const permissions: Array<string> = getPermi('AD_PERMI');
    if (value) {
      if (typeof value === 'string') {
        value = [value];
      }
      if (value && Array.isArray(value) && value.length > 0) {
        if (!permissions.some((v) => value.includes(v))) {
          el.parentNode && el.parentNode.removeChild(el);
        }
      }
    }
  },
};

/**
 * v-hasRole 角色权限处理
 */

export const hasRole = {
  mounted(el: HTMLElement, binding: { value: Array<string> | string }) {
    let { value } = binding;
    const roles: Array<string> = getPermi('AD_ROLE');
    if (value) {
      if (typeof value === 'string') {
        value = [value];
      }
      if (value && Array.isArray(value) && value.length > 0) {
        if (!roles.some((v) => value.includes(v))) {
          el.parentNode && el.parentNode.removeChild(el);
        }
      }
    }
  },
};
